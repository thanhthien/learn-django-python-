from django.conf.urls import url
from polls import views

app_name = 'polls'
urlpatterns = [
    url('^$',views.IndexView.as_view(), name='index'),
    url('^question/add/$', views.AddQuestion.as_view(), name='add-question'),
    url('^question/add/$', views.Add_1.as_view(), name='add'),
    url('(?P<question_id>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url('(?P<question_id>[0-9]+)/add-choice/$', views.AddChoice.as_view(), name='add-choice'),
    url('(?P<question_id>[0-9]+)/choice_a/$', views.choice_example.as_view(), name='choice_a'),
    url('(?P<question_id>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    url('(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    #url('(?P<question_id>[0-9]+)/choice_student/$', views.Add_Choice_Student.as_view(), name='choice_student'),

]