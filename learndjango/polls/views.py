# -*- coding: utf-8 -*-
import datetime
from audioop import reverse

from django.http import HttpResponse, HttpResponseRedirect, Http404

# Create your views here.
from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.urls import reverse
from django.utils import timezone
from django.views import generic
from django.views.generic import CreateView

from polls.models import Question, Choice

def detail(request, question_id):
    return HttpResponse("You're looking at question %s." % question_id)

def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)

#=========================#

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    output = ', '.join([q.question_text for q in latest_question_list])
    return HttpResponse(output)

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    template = loader.get_template('polls/index.html')
    context = {
        'latest_question_list': latest_question_list,
    }
    return HttpResponse(template.render(context, request))

#=========================#

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)

def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'polls/detail.html', {'question': question})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})

#=========================#


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]

    def get_context_data(self, **kwargs):
        context = generic.ListView.get_context_data(self, **kwargs)

        question_ids = Choice.objects.filter(votes__gt = 5).values_list('question_id', flat=True)
        context['questions'] = Question.objects.filter(id__in=question_ids)
        # context['questions'] = Question.objects.filter(choice__votes__gte=5)

                                    #===========#

        question_id1 = Choice.objects.filter(votes__lt = 6).values_list('question_id', flat=True)
        context['question_example'] = Question.objects.filter(id__in=question_id1)
        #context['question_example'] = Question.objects.filter(choice__votes__lt= 6)
                                    #===========#

        #context['questions'] = Question.objects.filter(pub_date__gt=datetime.datetime.now())
        context['question_pub_date'] = Question.objects.filter(pub_date__year = 2005)

        #=============== Delete object ==============

        Question.objects.filter(question_text = 'QƯE').delete()
        Choice.objects.filter(choice_text = 'dasd').delete()

        return context



class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    template_name = 'polls/results.html'
    model = Question

    pk_url_kwarg = 'question_id'


def get_queryset(self):
    """
    Return the last five published questions (not including those set to be
    published in the future).
    """
    return Question.objects.filter(
        pub_date__lte=timezone.now()
    ).order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    template_name = 'polls/detail.html'
    context_object_name = 'question'

    def get_object(self, queryset=None):
        return Question.objects.get(pk=self.kwargs['question_id'])


# =========================#


class AddQuestion(CreateView):
    model = Question
    fields = ['question_text','pub_date']
    template_name = 'polls/addquestion.html'

    def get_success_url(self):
        return reverse('polls:index')

class Add_1(CreateView):
    model = Question
    fields = ['question','pub_date']
    template_name = 'polls/add.html'

    def get_success_url(self):                    # The URL to redirect to when the form is successfully processed.
        return reverse('polls:index')             # Returns success_url by default.


class AddChoice(CreateView):
    model = Choice
    fields = ['choice_text']
    template_name = 'polls/addchoice.html'


    def form_valid(self, form):
        choice = form.save(False)
        choice.question_id = self.kwargs['question_id']
        choice.save()
        return HttpResponseRedirect(reverse('polls:detail', kwargs={'question_id':choice.question_id}))


class choice_example(CreateView):
    model = Choice
    fields = ['choice_text']
    template_name= 'polls/choice.html'

    def form_valid(self, form):                                     # Redirects to get_success_url().
        choice= form.save(False)                                    # not save in database.
        choice.question_id = self.kwargs['question_id']             # "self.kwagrs" to access the url parameters in class based view.
        choice.save()
        return HttpResponseRedirect(reverse('polls:detail', kwargs={'question_id':choice.question_id}))

'''
class Add_Choice_Student(CreateView):
    model = Student
    template_name = 'polls/Choice_student.html'

    def form_valid(self, form):
        Student= form.save(False)
        Student.question_id= self.kwargs['question_id']
        Student.save()
        return HttpResponseRedirect(reverse('polls:detail',kwargs={'question_id': Student.question_id}))
'''
